FROM node:18.18

RUN apt-get update && apt-get install -y \
	curl \
	git \
	p7zip \
	wine \
	&& \
	apt-get clean && \
	rm -rf /var/lib/apt/lists/*

RUN npm install -g @angular/cli@16.1.3 \
  && rm -rf /tmp/* *.tar.gz ~/.npm 


RUN mkdir -p /.npm && chmod -R a+rwx /.npm

expose 4200

workdir /src

